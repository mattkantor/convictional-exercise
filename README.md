# Answer (one of many possible)

This is one possible answer to the engineering challenge.  It is unopinionated but there is certainly room for improvement, including 
- dependency injection vs standard flask-like approach
- monitoring
- more logging
- makefile as features are added


## Getting started

###Requirements

- python3.6 or greater
- docker / docker-compose

###Setup

To run in development (locally)

```shell
$ export DSN='<your DSN>'
$ python3 -m venv venv && source venv/bin/activate
$ pip install -r requirements.txt
$ docker-compose up -d
$ alembic upgrade head
$ python import.py --file data/db.json
$ uvicorn app.main:app --reload
```

### running the suite


Other notes:

- API keys This might be somehting to add but running in a VPC (depending on your setup) if this is internal facing then
it may not be necessary.
- Cache: due to lack of requirements and the volitile nature of inventory items cache was not implemented.  
  Normally this would be a wrapper around KeyDB or redis non-persistent with appropriate TTLs.  
- Python was chosen due to expediency.  Obviously GoLang is far superior.  
- monitoring should be added (i.e. prometheus)

# Concerns

- not sure why you need product ID returned with inventory. Is there ever a use case to total up across variants?
- I appreciate the implication to have inventory as a one-to-one on variant. 
- This problem itself is fairly trivial - it makes one wonder why it was chosen for a test and why have a test at all.  IT would be easy to copy a bootstrap from github and fill in the pieces.  
- I'd argue there are more important things to discuss and this isnt  quick exercise (as it mentioned in the readme) unless youaare throwing it together.  Launching a microservice in real life takes a few days minumum if you do it right.