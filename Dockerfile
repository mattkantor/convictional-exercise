FROM pypy:3.7-7.3.5-buster

RUN apt-get update && apt-get -y install libpq-dev gcc

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DSN ''

EXPOSE 80

COPY . /app

RUN pip install -r requirements.txt
RUN alembic upgrade head
RUN python app/import.py --file data/db.json

CMD ["uvicorn", "main:app", "--host", "0.0.0.0","--workers", "1", "--port", "80"]