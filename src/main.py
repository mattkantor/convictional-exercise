import os
from src.app import App
from src.database_manager import DatabaseManager


database_dsn = os.environ.get("DSN", "postgresql+psycopg2://kantor:development@localhost:5432/convictional_development")
manager = DatabaseManager(dsn=database_dsn)
app = App(manager).app
