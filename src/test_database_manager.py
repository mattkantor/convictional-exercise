from typing import List, Dict
from src.model import Product, Inventory
import logging

logger = logging.getLogger(__name__)

test_image = dict(source='http://placekittchen.com/300/200')
test_variant = dict(id=1001, title="variant", images=[test_image])
test_product = dict(id=1001, code='TST', title="test", vendor="google", bodyHtml="<blink>hi</blink>", variants=[test_variant])
inventory = dict(product_id=1001, variant_id=1001, qty=2)


class TestDatabaseManager:

    def ping(self):
        return True

    def migrate(self):
        pass

    def add_product(self, product_id, product_json):
        return True

    def get_products(self) -> Dict[str, str, str, int]:
        return {"success": True, "data": [], 'status': 200, 'message': "OK"}

    def get_product(self, id: int) -> Dict[str, str, str, int]:
        return {"success": True, "data": {}, 'status': 200, 'message': "OK"}

    def get_inventory(self) -> List[Inventory]:
        return [inventory]
