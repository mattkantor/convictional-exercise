from typing import List
from src import App
from fastapi.testclient import TestClient
from src.model import Product
from src.test_database_manager import TestDatabaseManager, test_product

manager = TestDatabaseManager()
server = App(manager)
client = TestClient(server.app)


def test_read_health():
    response = client.get("/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


def test_read_home():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"welcome": "┳━┳ ヽ(ಠل͜ಠ)ﾉ"}


def test_get_products():
    response = client.get("/products")
    assert response.status_code == 200
    assert response.json() == [test_product]


def test_get_product():
    response = client.get("/products/1001")
    assert response.status_code == 200
    assert response.json() == test_product


def test_single_product():
    data = test_product

    assert True


def test_multiple_products():
    data = test_product
    assert False
