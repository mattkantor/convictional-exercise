from fastapi import FastAPI
from src.api import ApiRoutes
from src.database_manager import DatabaseManager
import logging

logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
logger.addHandler(handler)


class App:
    app = None
    router = None
    settings = None
    manager = None

    def __init__(self, manager: DatabaseManager):
        logger.info("loading FastAPI")
        self.app = FastAPI()
        self.manager = manager
        if not self.manager.ping():
            logger.exception("Could not connect to Database");
            exit(0)
        router = ApiRoutes(self.manager)
        self.routes(router)


    def routes(self, router):
        self.app.add_api_route(path="/",  endpoint=router.root, methods=["GET"])
        self.app.add_api_route(path="/health", endpoint=router.health, methods=["GET"])
        self.app.add_api_route(path="/products", endpoint=router.get_all_products,
                                methods=["GET"] )
        self.app.add_api_route(path="/products/{product_id}", endpoint=router.get_product_by_id,
                                methods=["GET"])

