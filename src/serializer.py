from marshmallow import Schema, fields

class ImageSchema(Schema):
    src = fields.Str()

class WeightSchema(Schema):
    weight = fields.Integer()
    unit = fields.Str()

class PriceSchema(Schema):
    price_currency_code = fields.Str()
    price_amount = fields.Str()

class VariantSchema(Schema):
    id = fields.Integer()
    title = fields.Str()
    product_id = fields.Integer()
    sku = fields.Str()
    position = fields.Integer()
    taxable = fields.Bool()
    barcode = fields.String()
    price = PriceSchema
    weight = WeightSchema
    image = fields.Nested(ImageSchema(), many=True)

class ProductSchema(Schema):
    id = fields.Integer()
    name = fields.Str()
    code = fields.Str()
    variants = fields.Nested(VariantSchema(), many=True)

class InventorySchema(Schema):
    product_id = fields.Integer()
    variant_id = fields.Integer()
    stock = fields.Integer()