import json
import os
from database_manager import DatabaseManager
from model import Product, Variant
import logging

logger = logging.getLogger(__name__)

class FileImporter:

    def __init__(self, path: str):
        self.data_path = path
        dsn = os.environ.get('DSN', None)
        self.database_manager = DatabaseManager(dsn)

    def ingest_data(self):
        try:
            with open(self.data_path, 'r') as file_pointer:
                filedb = json.load(file_pointer)
                products = filedb['products']
                for p in products:
                    self.database_manager.add_products(p)
        except Exception as e:
            print(e)
            logger.exception(e, exc_info=True)


if __name__ == '__main__':
    #TODO truncate flag
    print("importing...")
    f = FileImporter('./data/db.json')
    f.ingest_data()
    print("import complete")
