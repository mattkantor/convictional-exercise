from typing import  Dict
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from src.model import Product, Inventory, Variant, Image
import logging

logger = logging.getLogger(__name__)
DEFAULT_CURRENCY = "USD"

class DatabaseManager:
    session = None

    def __init__(self, dsn=None):
        self.engine = create_engine(dsn)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()


    def ping(self):
        with self.engine.connect() as connection:
            result = connection.execute("select 1=1")
        return True

    def add_products(self, product_rwa):
        try:

            product_dict = {k: v for k, v in product_rwa.items() if k in ['title', 'code', 'body_html', 'handle', 'product_type']}
            product = Product(**product_dict)


            for variant_raw in product_rwa['variants']:
                variant_dict = {k: v for k, v in variant_raw.items() if
                                k in ['v', 'title', 'title', 'sku', 'position','weight',
                                      'weight_unit']}

                variant = Variant(**variant_dict)
                variant.taxable = bool(variant_raw['taxable'])
                if "price" in variant_dict and 'currency_code' in variant_dict['price']:
                    variant.price_currency_code = variant_dict['price']['currency_code']
                    variant.price_amount = variant_dict['price']['amount']
                else:
                    variant.price_currency_code = DEFAULT_CURRENCY
                    variant.price_amount = 0

                product.variants.append(variant)
                images_dict = variant_raw['images']
                for image_dict in images_dict:
                    if "src" in image_dict:
                        image = Image(source=image_dict['src'])
                        variant.images.append(image)
            self.session.add(product)
            self.session.commit()
        except Exception as e:
            print(e)
            logger.exception(e, exc_info=True)







        return True

    def __get_joined_product_kids(self):
        products = self.session.query(Product, Variant, Image).join((Variant, Product.variants)).join(
            (Image, Variant.images))
        return products

    def get_products(self) -> Dict:
        try:
            #products = self.session.query(Product, Variant, Image).join((Variant, Product.variants)).join((Image, Variant.images)).all()
            products = self.__get_joined_product_kids().all()
            return {"success": True, "data": products, 'status': 200, 'message': 'OK'}
        except Exception as e:
            return {"success": False, "data": [], 'status': 500, 'message': str(e)}

    def get_product(self, id: int) -> Dict:
        try:
            product  = self.session.query(Product, Variant, Image).join((Variant, Product.variants)).join((Image, Variant.images)).filter_by(id=id).all()
            if product is None: return {'success': False, 'data': {}, 'status': 404, 'message': 'Not found'}

            return {"success": True, "data": product, 'status': 200}

        except Exception as e:
            return {'success': False, "data":{}, 'status': 500, 'message': str(e)}

    def get_inventory(self) -> Dict:
        inventories = self.session.Query(Inventory).filter_by(Inventory.stock > 0).all()
        return {"success": True, "data": inventories}
