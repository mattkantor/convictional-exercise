from fastapi import APIRouter, Path
from typing import List, Dict
from src.model import Product, Inventory
from src.serializer import ProductSchema


class ApiRoutes:

    def __init__(self, database_manager):
        self.manager = database_manager

    def root(self) -> Dict[str, str]:
        return {"welcome": "┳━┳ ヽ(ಠل͜ಠ)ﾉ"}

    def health(self) -> Dict[str, str]:
        return {"status": "OK"}

    def get_all_products(self) :
        product_hash = self.manager.get_products()
        schema = ProductSchema()
        return schema.dump(product_hash['data'], many=True)
        #return products['data']

    def get_product_by_id(self, product_id: int = Path(..., alias='product_id', ge=0)) -> Product:
        product: Product = self.manager.get_product(product_id)
        return product

    def get_inventory(self) -> List[Inventory]:
        """
        Returns product inventories
        """
        pass
