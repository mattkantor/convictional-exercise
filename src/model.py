from __future__ import annotations
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from sqlalchemy.orm import declarative_base, relationship

base = declarative_base()


class Weight:
    value: int
    unit: str

class Image(base):
    __tablename__ = 'image'
    id = Column(Integer, primary_key=True)
    source = Column(String)
    variant_id = Column(Integer, ForeignKey("variant.id"))
    variant = relationship("Variant",foreign_keys=[variant_id])


class Variant(base):
    __tablename__ = 'variant'

    id = Column(Integer, primary_key=True)
    product_id = Column(Integer, ForeignKey("product.id"))
    title = Column(String)
    sku = Column(String)
    position = Column(Integer)
    taxable = Column(Boolean)
    weight_unit = Column(String)
    weight = Column(Integer)
    price_currency_code = Column(String)
    price_amount = Column(String) # comes in as a string but should it be a string?

    @property
    def price(self):
        return { 'currency_code': self.price_currency_code, 'amount': self.price_amount}

    product = relationship("Product", foreign_keys=[product_id])
    images = relationship("Image", back_populates="variant", cascade="all, delete-orphan")
    inventories = relationship("Inventory", cascade="all, delete-orphan")


class Inventory(base):
    __tablename__ = 'inventory'

    id = Column(Integer, primary_key=True)
    variant_id = Column(Integer, ForeignKey("variant.id"))
    product_id = Column(Integer, ForeignKey('product.id'))
    stock = Column(Integer)
    variant = relationship("Variant", foreign_keys=[variant_id])
    variant = relationship("Product", foreign_keys=[product_id])


class Product(base):
    __tablename__ = 'product'
    id = Column(Integer, primary_key=True)
    code= Column(String)
    title = Column(String)
    vendor = Column(String)
    body_html = Column(String)
    handle = Column(String)
    product_type = Column(String)

    variants = relationship("Variant", back_populates="product", cascade="all, delete-orphan")

class Error:
    message: str
